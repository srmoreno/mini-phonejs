/* Copyright (C) 2018 Freetech Solutions */

class PhoneJS {
    constructor(agent_id, sip_extension, sip_secret,
                websocket_server, websocket_port, asterisk_server,
                local_audio, remote_audio) {
        /* Config */
        this.agent_id = agent_id
        this.sip_extension = sip_extension;
        this.sip_secret = sip_secret;
        this.websocket_server = websocket_server;
        this.websocket_port = websocket_port;
        this.asterisk_server = asterisk_server;

        /* Components / Colaborators */
        this.userAgent = undefined;
        this.currentSession = undefined;

        this.local_audio = local_audio;
        this.remote_audio = remote_audio;

        /* Local Variables */
        this.callTimeoutHandler = undefined;
        this.transferTimeoutHandler = undefined;

        /* eventsCallbacks */
        this.eventsCallbacks = {
            onUserAgentRegistered: $.Callbacks(),
            onUserAgentDisconnect: $.Callbacks(),
            onUserAgentRegisterFail: $.Callbacks(),

            onCallConnected:  $.Callbacks(),
            onOutCallFailed: $.Callbacks(),
            onCallEnded: $.Callbacks(),

            onSessionFailed: $.Callbacks(),

        }
    }

    startSipSession() {
        var config = {
                uri: "sip:" + this.sip_extension + "@" + this.asterisk_server,
                ws_servers: "wss://" + this.websocket_server + ":" + this.websocket_port + "/ws",
                password: this.sip_secret,
                realm: this.websocket_server,
                hack_ip_in_contact: true,
                session_timers: false,
                register_expires: 120,
                pcConfig: {
                    rtcpMuxPolicy: 'negotiate'
                }
            };

        // Inicializar Sesion de Websocket con Kamailio  "Hacer Login"
        if (this.sip_extension && this.sip_secret) {
            this.userAgent = new JsSIP.UA(config);
            this.userAgent.start();
            this.subscribeToUserAgentEvents();
        }
    }

    logout() {
        var options = {
            all: true
        };
        this.userAgent.unregister(options);
    }

    /******  Eventos User Agent  *******/
    subscribeToUserAgentEvents() {
        var self = this;
        //Connects to the WebSocket server
        this.userAgent.on("registered", function(e) { // cuando se registra la entidad SIP
            console.log('User Agent: registered');
            self.eventsCallbacks.onUserAgentRegistered.fire();
        });

        this.userAgent.on("disconnected", function(e) {
            console.log('User Agent: disconnected');
            self.eventsCallbacks.onUserAgentDisconnect.fire();
        });

        this.userAgent.on("registrationFailed", function(e) { // cuando falla la registracion
            console.log('User Agent: registrationFailed');
            self.eventsCallbacks.onUserAgentRegisterFail.fire();
        });

        /*
           La sesion se crea al: Llamar, Hacer Login
        */
        this.userAgent.on("newRTCSession", function(e) {
            console.log('newRTCSession');
            self.invite_request = e.request;
            self.currentSession = e.session;

            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // Session Events
            self.currentSession.on("failed", function(e) {
                console.log('session: failed');
                self.Sounds("", "stop");
                self.eventsCallbacks.onSessionFailed.fire();
            });

            self.currentSession.on("confirmed", function(e) {
                // Aca si puedo decir que esta establecida
                console.log('session: confirmed');
                self.eventsCallbacks.onCallConnected.fire();
            });

            self.currentSession.on("accepted", function() { // cuando se establece una llamada
                console.log('session: accepted');
                self.Sounds("", "stop");
            });

            self.currentSession.on("ended", function() { 
                // Cuando Finaliza la llamada. Ya se maneja desde el evento de la llamada
                console.log('session: ended');
            });
        });
    };

    /* FUNCTIONS */

    makeCall(number_to_call) {
        var self = this;
        console.log('makeCall: ' + number_to_call);

        // Luego de 60 segundos sin respuesta, stop al ringback y cuelga discado
        // TODO: Cancelar el timeout una vez recibida respuesta ('accepted', failed', )
        this.callTimeoutHandler = setTimeout(function() {self.hangUp();}, 61000);

        var eventHandlers = {
            // TODO: Verificar si no hay otros posibles eventos.
            // Asegurarse de que cualquier finalizacion termina llamando al clearTimeout(...)
            'confirmed': function(e) {
                console.log('makeCall: confirmed');
                clearTimeout(self.callTimeoutHandler);
                self.local_audio.srcObject = self.currentSession.connection.getLocalStreams()[0];
            },
            'addstream': function(e) {
                console.log('makeCall: addstream');
                clearTimeout(self.callTimeoutHandler);
                // Attach remote stream to remoteView
                var stream = e.stream;
                self.remote_audio.srcObject = stream;
            },
            'failed': function(data) {
                console.log('makeCall: failed - ' + number_to_call);
                clearTimeout(self.callTimeoutHandler);
                self.eventsCallbacks.onOutCallFailed.fire(data.cause);
                if (data.cause === JsSIP.C.causes.BUSY) {
                    self.Sounds("", "stop");
                    self.Sounds("", "play");
                }
            },
            'ended': function(data) {
                console.log('Call ENDED')
                self.eventsCallbacks.onCallEnded.fire();
            },
        };
        var opciones = {
            'eventHandlers': eventHandlers,
            'mediaConstraints': {
                'audio': true,
                'video': false
            },
            pcConfig: {
                rtcpMuxPolicy: 'negotiate'
            }
        };

        // Finalmente Mando el invite/llamada
        console.log("sip:" + number_to_call + "@" + this.asterisk_server)
        this.userAgent.call("sip:" + number_to_call + "@" + this.asterisk_server, opciones);
    }

    ///----------

    // TODO: Hacer que suene como que esta llamando??
    Sounds(callType, action) {
        var ring = null;
        if (action === "play") {
            if (callType === "Out") {
                ring = document.getElementById('RingOut');
                ring.play();
                ring.onended = function() {
                    ring.play();
                };
            } else {
                ring = document.getElementById('RingBusy');
                ring.play();
            }
        } else {
            ring = document.getElementById('RingOut');
            ring.pause();
            ring = document.getElementById('RingBusy');
            ring.pause();
        }
    }

    cleanLastCallData() {
        self.currentSession = undefined;
    }

    hangUp(play_stop_sound=true) {
        if (play_stop_sound) {
            this.Sounds("", "stop");
        }
        this.userAgent.terminateSessions();
    }

};
