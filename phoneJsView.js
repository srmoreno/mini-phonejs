
/* Copyright (C) 2018 Freetech Solutions */

/*
 * Phone View for use with PhoneJSController 
 */
class PhoneJSView {
    constructor () {
        /* Streams */
        this.local_audio = $('#localAudio')[0];
        this.remote_audio = $('#remoteAudio')[0];

        /* Inputs */
        this.callButton = $("#call");
        this.hangUpButton = $("#endCall");
        this.numberDisplay = $("#number_to_call");

        this.keypad_buttons_ids = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                                   'asterisk', 'hashtag']
        this.inputs_ids = ["call", "endCall"]
        
        /* Outputs */
        // this.callStatus = $('#CallStatus');

        this.startKeypad();
    };

    startKeypad() {
        var self = this;
        $(".key").click(function(e) {
            var pressed_key = e.currentTarget.childNodes[0].data;
            var dialedNumber = self.numberDisplay.value;
            self.numberDisplay.value = dialedNumber + pressed_key;
        });
    }

    disable(elements) {
        this.setDisabledProp(elements, true);
    }

    enable(elements) {
        this.setDisabledProp(elements, true);
    }

    setDisabledProp(elements, property) {
        for (var i=0; i < elements.length; i++) {
            var element = elements[i];
            element.prop('disabled', property);
        }
    }

    setInputDisabledStatus(state_name) {
        var status_config = this.getStateConfig(state_name);
        this.setKeypadButtonsEnabled(status_config.keypad_enabled);
        this.setInputsEnabled(status_config.enabled_buttons);
    }

    setKeypadButtonsEnabled(enabled) {
        for (var i=0; i<this.keypad_buttons_ids.length; i++) {
            var id = this.keypad_buttons_ids[i];
            $(`#${id}`).prop('disabled', !enabled);
        }
    }

    setInputsEnabled(enabled_ones) {
        for (var i=0; i<this.inputs_ids.length; i++) {
            var id = this.inputs_ids[i];
            $(`#${id}`).prop('disabled', enabled_ones.indexOf(id) == -1);
        }
    }

    getStateConfig(state_name) {
        return PHONE_STATUS_CONFIGS[state_name]
    }
}

var PHONE_STATUS_CONFIGS = {
    'Inactive': {
        keypad_enabled: false,
        enabled_buttons: ['call'],
    },
    'Connecting': {
        keypad_enabled: false,
        enabled_buttons: ['endCall'],
    },
    'LoggingIn': {
        keypad_enabled: false,
        enabled_buttons: ['endCall'],
    },
    'Calling': {
        keypad_enabled: false,
        enabled_buttons: ['endCall'],
    },
    'OnCall': {
        keypad_enabled: true,
        enabled_buttons: ['endCall'],
    },
}
