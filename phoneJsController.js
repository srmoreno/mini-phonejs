/* Copyright (C) 2018 Freetech Solutions */

/* Requirements:            */
/*      - config.js         */
/*      - phoneJsFSM.js     */
/*      - miniPhoneJs.js    */



class PhoneJSController {
    // Connects PhoneJS with a PhoneJSView.
    constructor() {
        var agent_id = miniPhoneConfig.agent_id;
        var sip_extension = miniPhoneConfig.sip_extension;
        var sip_secret = miniPhoneConfig.sip_secret;
        var websocket_server = miniPhoneConfig.websocket_server;
        var websocket_port = miniPhoneConfig.websocket_port;
        var asterisk_server = miniPhoneConfig.asterisk_server;

        this.number_to_call = miniPhoneConfig.number_to_call;
        this.view = new PhoneJSView();
        this.phone = new PhoneJS(agent_id, sip_extension, sip_secret,
                                 websocket_server, websocket_port, asterisk_server,
                                 this.view.local_audio, this.view.remote_audio);
        this.phone_fsm = new PhoneFSM();

        this.subscribeToViewEvents();
        this.subscribeToFSMEvents();
        this.subscribeToPhoneEvents();
        this.view.setInputDisabledStatus('Inactive');
    }

    subscribeToViewEvents() {
        var self = this;

        this.view.callButton.click(function(e) {
            navigator.permissions.query({name: 'microphone'})
            self.makeCall();
        });

        this.view.hangUpButton.click(function() {
            self.phone.hangUp();
        });

        this.subscribeToKeypadEvents();
    }

    subscribeToKeypadEvents() {
        /* Botones de telefono */
        // TODO: Va a tener teclado?
        var self = this;
        $(".key").click(function(e) {
            var pressed_key = e.currentTarget.childNodes[0].data;
            if (self.phone_fsm.state == 'OnCall'){
                self.phone.currentSession.sendDTMF(pressed_key);
            }            
        });
    }

    subscribeToFSMEvents() {
        var self = this;
        this.phone_fsm.observe({
            onInactive: function() {
                console.log('FSM: onInactive')
                self.view.setInputDisabledStatus('Inactive');
                // TODO: Mostrar boton para llamar
                // deshabilitar Teclado
            },
            onConnecting: function() {
                console.log('FSM: onConnecting')
                self.view.setInputDisabledStatus('Connecting');
                // TODO: Mostrar que esta llamando
                // deshabilitar Teclado
                self.phone.startSipSession();
            },
            onCalling: function() {
                console.log('FSM: onCalling')
                self.view.setInputDisabledStatus('Calling');
                self.phone.makeCall(self.number_to_call);
                // TODO: Seguir mostrando que esta llamando
                // deshabilitar Teclado
            },
            onOncall: function() {
                console.log('FSM: onOncall')
                self.view.setInputDisabledStatus('OnCall');
                // TODO: Mostrar que esta hablando
                // Habilitar Teclado
            },
        });
    }

    subscribeToPhoneEvents() {
        var self = this;

        /** User Agent **/
        this.phone.eventsCallbacks.onUserAgentRegistered.add(function () {
            console.log('onUserAgentRegistered');
            self.phone_fsm.call();
        });

        this.phone.eventsCallbacks.onUserAgentRegisterFail.add(function () {
            // TODO: Mostrar mensaje de error.
            self.phone_fsm.failedRegistration();
        });

        this.phone.eventsCallbacks.onUserAgentDisconnect.add(function () {
            // TODO: Mostrar mensaje de error.
            // TODO: Definir acciones a tomar.
        });

        /** Calls **/
        this.phone.eventsCallbacks.onSessionFailed.add(function() {
            // TODO: Mostrar mensaje de error.
            console.log('onSessionFailed');
            if (self.phone_fsm.state == 'Inactive'){ // Fallo el Register
                // Posiblemente haya fallado la sesion.
            } else if (self.phone_fsm.state == 'Connecting'){ // Fallo el Register
                self.phone_fsm.failedRegistration();
            } else if (self.phone_fsm.state == 'Calling'){ // Fallo la llamada
                self.phone_fsm.endCall();
                self.phone.cleanLastCallData();     // Es necesario?
            } else {
                console.log(`Session Failed at: ${self.phone_fsm.state}`)
            }
        });

        // Outbound Call
        this.phone.eventsCallbacks.onCallConnected.add(function() {
            console.log('onCallConnected from: ' + self.phone_fsm.state);
            // TODO: Mostrar que se conectó.
            if (self.phone_fsm.state == 'Calling') {
                self.phone_fsm.connectCall();
            } else {console.log(`Error: onCallConnected at: ${self.phone_fsm.state}`)}
        });

        this.phone.eventsCallbacks.onOutCallFailed.add(function(cause) {
            // TODO: Mostrar mensaje de error.
            console.log('--->  onOutCallFailed')
            self.logCallFailedStatus(cause);
            if (self.phone_fsm.state == 'Connecting') {
                self.phone_fsm.endCall();
                self.phone_fsm.failedRegistration();
            }
            if (self.phone_fsm.state == 'Calling') {
                self.phone_fsm.endCall()
            }
            // El fallo tambien dispara el onSessionFailed?? Si un fallo en 
            // onSessionFailed despues dispara este
        });

        this.phone.eventsCallbacks.onCallEnded.add(function() {
            // TODO: Mostrar mensaje de que terminó la llamada
            self.phone_fsm.endCall();
            self.phone.cleanLastCallData();     // Es necesario?
        });
    }

    makeCall() {
        this.phone_fsm.startCallProcess();
        //  TODO: Mostrar que esta llamando
    }

    logCallFailedStatus(cause) {
        switch(cause){
            case JsSIP.C.causes.BUSY:
                console.log("Number busy, try later", "orange");
                break;
            case JsSIP.C.causes.REJECTED:
                console.log("Rejected, try later", "orange");
                break;
            case JsSIP.C.causes.UNAVAILABLE:
                console.log("Unavailable, contact your administrator", "red");
                break;
            case JsSIP.C.causes.NOT_FOUND:
                console.log("Error, check the number dialed", "red");
                break;
            case JsSIP.C.causes.AUTHENTICATION_ERROR:
                console.log("Authentication error, contact your administrator", "red");
                break;
            case JsSIP.C.causes.MISSING_SDP:
                console.log("Error, Missing sdp", "red");
                break;
            case JsSIP.C.causes.ADDRESS_INCOMPLETE:
                console.log("Address incomplete", "red");
                break;
            case JsSIP.C.causes.SIP_FAILURE_CODE:
                console.log("Service Unavailable, contact your administrator", "red");
                break;
            case JsSIP.C.causes.USER_DENIED_MEDIA_ACCESS:
                console.log("WebRTC Error: User denied media access", "red");
                break;
            default:
                console.log("Error: Call failed", "red");
        }
    }

}
