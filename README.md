# Mini PhoneJs

Minimal PhoneJS. Single manual call.

Requires valid settings values in config.js: for agent_id, sip_extension, sip_secret, 
websocket_server, websocket_port, asterisk_server and number_to_call.